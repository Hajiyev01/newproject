package az.ingress.BookStore.service.impl;

import az.ingress.BookStore.dto.BookDto;
import az.ingress.BookStore.entity.Author;
import az.ingress.BookStore.entity.Book;
import az.ingress.BookStore.entity.User;
import az.ingress.BookStore.exception.UnAuthorizeException;
import az.ingress.BookStore.repository.BookRepository;
import az.ingress.BookStore.repository.UserRepository;
import az.ingress.BookStore.service.UserService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final BookRepository bookRepository;

    @Override
    public boolean isUserActive(Long userId) throws UnAuthorizeException {
      User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User Not Faund."));
        return  user.isActive();
    }

    @Override
    public List<Book> getAllBooks(Long userId) throws UnAuthorizeException {
        boolean isUserActive = isUserActive(userId);
        if (!isUserActive) {
            throw new UnAuthorizeException("Account not active.");
        }
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User Not Faund."));

        return user.getBooks();
    }

    @Override
    public void registerBooks(Long userId, BookDto bookDto) throws UnAuthorizeException {

        boolean isUserActive = isUserActive(userId);
        if (!isUserActive) {
            throw new UnAuthorizeException("Account not active.");
        }
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User Not Faund."));
        Book book = bookRepository.findByBookName(bookDto.getBookName());

       if (book== null){
           Book book1 = new Book();
           book1.setBookName(bookDto.getBookName());
           book1.getUsers().add(user);
           user.getBooks().add(book1);

           userRepository.save(user);
           bookRepository.save(book1);
       }
        else {
            book.getUsers().add(user);
            user.getBooks().add(book);
            userRepository.save(user);

       }
    }
}
