package az.ingress.BookStore.service;

import az.ingress.BookStore.dto.BookDto;
import az.ingress.BookStore.entity.Book;
import az.ingress.BookStore.exception.UnAuthorizeException;
import org.springframework.stereotype.Service;

import java.nio.file.AccessDeniedException;
import java.util.List;


public interface AuthorService {

    boolean isAuthorActive(Long authorId);

    void save(Long authorId, BookDto bookDto) throws UnAuthorizeException;

    void update(Long authorId, Long bookId, BookDto bookDto) throws UnAuthorizeException;

    void delete(Long authorId, Long bookId) throws UnAuthorizeException, AccessDeniedException;

    List<Book> getAllBooks(Long authorId) throws UnAuthorizeException;
}
