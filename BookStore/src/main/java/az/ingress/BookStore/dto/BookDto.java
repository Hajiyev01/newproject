package az.ingress.BookStore.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookDto {
    String bookName;
    String title;
    String categories;
    String bookYear;


}
