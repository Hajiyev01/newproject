package az.ingress.BookStore.controller;

import az.ingress.BookStore.dto.BookDto;
import az.ingress.BookStore.entity.Book;
import az.ingress.BookStore.exception.UnAuthorizeException;
import az.ingress.BookStore.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.nio.file.AccessDeniedException;
import java.util.List;

@RestController
@RequestMapping("/author")
@RequiredArgsConstructor
public class AuthorController {

private final AuthorService authorService;

    @PostMapping("/{authorId}/books")
    public ResponseEntity<String> saveBook(@PathVariable Long authorId, @RequestBody BookDto bookDto) throws UnAuthorizeException {

        boolean isAuthorActive = authorService.isAuthorActive(authorId);

        if (!isAuthorActive){
            return  ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Account not active.");
        }
        authorService.save(authorId,bookDto);
        return ResponseEntity.ok("Book saved.");
    }

@PutMapping("/{authorId}/books/{bookId}")
    public ResponseEntity<String> updateBook(@PathVariable  Long authorId,
                                             @PathVariable Long bookId,
                                             @RequestBody BookDto bookDto) throws UnAuthorizeException {
    boolean isAuthorActive = authorService.isAuthorActive(authorId);

    if (!isAuthorActive){
        return  ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Account not active.");
    }
        authorService.update(authorId,bookId,bookDto);
    return ResponseEntity.ok("Books saved.");


}
@DeleteMapping("/{authorId}/books/{bookId}")
 public ResponseEntity<String> deleteBook(@PathVariable  Long authorId,
                                          @PathVariable Long bookId) throws AccessDeniedException, UnAuthorizeException {

    boolean isAuthorActive = authorService.isAuthorActive(authorId);

    if (!isAuthorActive){
        return  ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Account not active.");
    }
    authorService.delete(authorId,bookId);
    return ResponseEntity.ok("Books deleted.");

}


@GetMapping("/{authorId}/books")
public ResponseEntity<List<Book>> getBook(@PathVariable Long authorId) throws UnAuthorizeException {

    boolean isAuthorActive = authorService.isAuthorActive(authorId);

    if (!isAuthorActive){
         ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Account not active.");
    }
   List<Book>  books = authorService.getAllBooks(authorId);

    return ResponseEntity.ok(books);
}


}
