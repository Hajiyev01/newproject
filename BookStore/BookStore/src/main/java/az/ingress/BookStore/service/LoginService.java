package az.ingress.BookStore.service;

public interface LoginService {

    boolean authenticate(String email, String password, String userType);
}
