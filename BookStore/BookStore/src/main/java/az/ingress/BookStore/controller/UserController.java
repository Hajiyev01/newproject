package az.ingress.BookStore.controller;

import az.ingress.BookStore.dto.BookDto;
import az.ingress.BookStore.dto.UserDto;
import az.ingress.BookStore.entity.Book;
import az.ingress.BookStore.exception.UnAuthorizeException;
import az.ingress.BookStore.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/bookstore/users")
public class UserController {

    private  final UserService userService;

    @GetMapping("/{userId}/books")
    public ResponseEntity<List<Book>> getAllBooks(@PathVariable Long userId) throws UnAuthorizeException {

        boolean isUserActive =userService.isUserActive(userId);
        if (!isUserActive){
           ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Account not active");
        }
        List<Book> books=userService.getAllBooks(userId);
        return ResponseEntity.ok(books);

    }

@PostMapping("/{userId}/books")
    public ResponseEntity<String> saveBooks(@PathVariable Long userId, @RequestBody BookDto bookDto) throws UnAuthorizeException {
    boolean isUserActive =userService.isUserActive(userId);
    if (!isUserActive){
        ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Account not active");

    }
    userService.registerBooks(userId,bookDto);

    return ResponseEntity.ok("Book register.");

}

}
