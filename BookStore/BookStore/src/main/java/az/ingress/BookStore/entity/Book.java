package az.ingress.BookStore.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String bookName;
    String title;
    String categories;
    String bookYear;

    @ManyToOne
    Author author;

    @ManyToMany(mappedBy = "books")
    List<User>users = new ArrayList<>();



}
