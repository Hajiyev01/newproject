package az.ingress.BookStore.repository;

import az.ingress.BookStore.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book,Long> {

    Book findByBookName(String bookName);

}
