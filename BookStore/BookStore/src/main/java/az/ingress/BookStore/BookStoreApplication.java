package az.ingress.BookStore;

import az.ingress.BookStore.entity.Book;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class BookStoreApplication {


	public static void main(String[] args) {
		SpringApplication.run(BookStoreApplication.class, args);

	}
}

