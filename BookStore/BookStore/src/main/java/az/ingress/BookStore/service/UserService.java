package az.ingress.BookStore.service;

import az.ingress.BookStore.dto.BookDto;
import az.ingress.BookStore.entity.Book;
import az.ingress.BookStore.exception.UnAuthorizeException;

import java.util.List;

public interface UserService {
    boolean isUserActive(Long userId) throws UnAuthorizeException;

    List<Book> getAllBooks(Long userId) throws UnAuthorizeException;

    void registerBooks(Long userId, BookDto bookDto) throws UnAuthorizeException;
}
