package az.ingress.BookStore.controller;

import az.ingress.BookStore.dto.LoginDto;
import az.ingress.BookStore.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bookstore")
@RequiredArgsConstructor
public class LoginController {


    private final LoginService loginService;

    @PostMapping("login")
    public ResponseEntity<String> responseEntity(@RequestBody LoginDto loginDto){
        boolean succesfully = loginService.authenticate(loginDto.getEmail(),loginDto.getPassword(),loginDto.getUserType());
        if (succesfully){
        return ResponseEntity.ok("Login successfully.");
        }
        else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Login Failed . ");
        }
    }
}
