package az.ingress.Market.repository;

import az.ingress.Market.dto.MarketResponseDto;
import az.ingress.Market.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarketRepository extends JpaRepository<Market, Long> {

}
