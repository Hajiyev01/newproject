package az.ingress.Market.repository;

import az.ingress.Market.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BranchRepository extends JpaRepository<Branch, Long> {

    @Query(value = "select * from branch b inner join market m on m.id = b.m_id inner join address a on b.a_id = a.id", nativeQuery = true)
    Branch getBranch();

    @Query(value = "select b from Branch b join fetch Market m join fetch Address a")
    Branch getBranch2();
}
