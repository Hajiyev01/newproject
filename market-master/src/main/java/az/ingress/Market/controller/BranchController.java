package az.ingress.Market.controller;

import az.ingress.Market.dto.RequestBranchDto;
import az.ingress.Market.model.Branch;
import az.ingress.Market.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {
    private final BranchService branchService;

    @PostMapping
    public long saveBranch(@RequestBody RequestBranchDto request) {
        return branchService.saveBranch(request);
    }

    @GetMapping
    public Branch getBranch() {
        return branchService.getBranch();
    }
}
