package az.ingress.Market.service.impl;

import az.ingress.Market.dto.RequestBranchDto;
import az.ingress.Market.model.Address;
import az.ingress.Market.model.Branch;
import az.ingress.Market.model.Market;
import az.ingress.Market.repository.BranchRepository;
import az.ingress.Market.service.AddressService;
import az.ingress.Market.service.BranchService;
import az.ingress.Market.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final MarketService marketService;
    private final AddressService addressService;
    private final ModelMapper modelMapper;
    @Override
    public long saveBranch(RequestBranchDto request) {
        Address address = addressService.findById(request.getAddress_id());
        Market market=marketService.getMarketWithId(request.getMarket_id());
        Branch branch = Branch.builder()
                .branchName(request.getBranchName())
                .address(address)
                .market(market)
                .build();
        return branchRepository.save(branch).getId();
    }

    @Override
    public Branch getBranch() {
        Branch branch = branchRepository.getBranch2();
        System.out.println(branch);
        return branch;
    }
}
