package az.ingress.Market.service;

import az.ingress.Market.dto.RequestBranchDto;
import az.ingress.Market.model.Branch;

public interface BranchService {
    long saveBranch(RequestBranchDto request);

    Branch getBranch();
}
