package az.ingress.Market.service;

import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.dto.MarketResponseDto;
import az.ingress.Market.model.Market;

import java.util.List;

public interface MarketService {
    Long createMarket(MarketRequestDto request);

    List<MarketResponseDto> getMarket();

    Market getMarketWithId(Long id);
}
