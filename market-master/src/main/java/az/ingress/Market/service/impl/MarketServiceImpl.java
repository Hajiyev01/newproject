package az.ingress.Market.service.impl;

import az.ingress.Market.config.AppConfiguration;
import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.dto.MarketResponseDto;
import az.ingress.Market.model.Market;
import az.ingress.Market.repository.MarketRepository;
import az.ingress.Market.service.MarketService;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {

    private final MarketRepository marketRepository;
    private final AppConfiguration appConfiguration;

    @Override
    public Long createMarket(MarketRequestDto request) {
        Market market = appConfiguration.getMapper().map(request, Market.class);
        return marketRepository.save(market).getId();
    }

    @Override
    public List<MarketResponseDto> getMarket() {
        List<Market> marketList = marketRepository.findAll();
        List<MarketResponseDto> marketResponseDtoList = new ArrayList<>();
        marketList.forEach(entity -> {
            MarketResponseDto marketResponseDto =
                    appConfiguration.getMapper().map(entity, MarketResponseDto.class);
            marketResponseDtoList.add(marketResponseDto);
        });
        return marketResponseDtoList;
    }

    @Override
    public Market getMarketWithId(Long id) {
        return marketRepository.findById(id).orElseThrow(()->new RuntimeException("Market not found with id" +id));
    }
}
