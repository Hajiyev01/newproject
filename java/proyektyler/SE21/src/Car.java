public class Car {

    char colour;
    int engineVolume;


    public static void main(String[] args) {

        Car b = new Car();

        Car firstQ7 = new Car();
        firstQ7.colour = 'R';
        firstQ7.engineVolume = 3000;

        System.out.println("Colour:" + firstQ7.colour);
        System.out.println("EngineVolume:" + firstQ7.engineVolume);
        Car secondQ7 = new Car();
        secondQ7.engineVolume = 3500;
        System.out.println("Engine:" + secondQ7.engineVolume);

        secondQ7.colour = 'Y';
        System.out.println("Colour:" + secondQ7.colour);

    }
}