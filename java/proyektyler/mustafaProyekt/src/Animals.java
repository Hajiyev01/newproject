public abstract class Animals {

    private int speed;

    private int foot;

    private String colour;

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getFoot() {
        return foot;
    }

    public void setFoot(int foot) {
        this.foot = foot;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }


    //constrctor

    public Animals(int speed, int foot, String colour) {
        this.speed = speed;
        this.foot = foot;
        this.colour = colour;


    }

    public void capEt() {
        System.out.println("Speed: " + speed);
        System.out.println("Foot: " + foot);
        System.out.println("Colour: " + colour);
    }


}
