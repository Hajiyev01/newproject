package az.ingress.BookStore.controller;

import az.ingress.BookStore.dto.RegisterDto;
import az.ingress.BookStore.service.RegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/bookstore")
public class RegisterController {

   private final RegisterService registerService;

   @PostMapping("register")
   public ResponseEntity<String> register(@RequestBody RegisterDto registerDto){
      registerService.register(registerDto);
      return ResponseEntity.ok("Register succesfully");
   }





}
