package az.ingress.BookStore.service;

import az.ingress.BookStore.dto.RegisterDto;

public interface RegisterService  {
    void register(RegisterDto registerDto);
}
