package az.ingress.BookStore.service.impl;

import az.ingress.BookStore.controller.LoginController;
import az.ingress.BookStore.entity.Author;
import az.ingress.BookStore.entity.User;
import az.ingress.BookStore.repository.AuthorRepository;
import az.ingress.BookStore.repository.UserRepository;
import az.ingress.BookStore.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

private final UserRepository userRepository;
private final AuthorRepository authorRepository;

    @Override
    public boolean authenticate(String email, String password, String userType) {

        Optional<Author> authorOptional = authorRepository.findByEmail(email);
        Optional<User> userOptional = userRepository.findByEmail(email);

        if (userOptional.isPresent()){
            User user = userOptional.get();
            if (password.equals(user.getPassword()) && !user.isActive() && userType.equals("user")){

                user.setActive(true);
                userRepository.save(user);
                return true;
            }
        }
        else if (authorOptional.isPresent()){
            Author author = authorOptional.get();
            if (password.equals(author.getPassword()) && !author.isActive() && userType.equals("author")){
                author.setActive(true);
                authorRepository.save(author);
                return true;
            }
        }

        return false;


    }
}
