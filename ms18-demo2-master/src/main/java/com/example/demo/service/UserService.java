package com.example.demo.service;

import com.example.demo.dto.LoginUserRequestDto;
import com.example.demo.dto.UserRequestDto;

public interface UserService {
    Long saveUser(UserRequestDto userRequestDto);

    String loginUser(LoginUserRequestDto loginUserRequestDto);

    String loginUser2(LoginUserRequestDto loginUserRequestDto);
}
