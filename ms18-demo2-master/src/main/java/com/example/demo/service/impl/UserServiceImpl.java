package com.example.demo.service.impl;

import com.example.demo.config.AppConfiguration;
import com.example.demo.dto.LoginUserRequestDto;
import com.example.demo.dto.UserRequestDto;
import com.example.demo.model.User;
import com.example.demo.repo.UserRepository;
import com.example.demo.service.UserService;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AppConfiguration appConfiguration;

    @Override
    public Long saveUser(UserRequestDto userRequestDto) {
        User user = appConfiguration.getMapper().map(userRequestDto, User.class);
        return userRepository.save(user).getId();
    }

    @Override
    public String loginUser(LoginUserRequestDto loginUserRequestDto) {
        Optional<User> optionalUser = userRepository.findByEmail(loginUserRequestDto.getEmail());
        optionalUser.ifPresent(user -> {
                    if (!user.getPassword().equals(loginUserRequestDto.getPassword())) {
                        throw new RuntimeException("Password is incorrect");
                    }
        }
        );
        return "Successfully login";
    }

    @Override
    public String loginUser2(LoginUserRequestDto loginUserRequestDto) {
        Optional<List<User>> optionalUser = userRepository.findByEmails(loginUserRequestDto.getEmail());
        log.info("Get users: {}", optionalUser.get());
        optionalUser.ifPresent(users -> {
            for (User user : users) {
                if (!user.getPassword().equals(loginUserRequestDto.getPassword())) {
                    throw new RuntimeException("Password is incorrect");
                }
            }
        }
        );
        return null;
    }
}
