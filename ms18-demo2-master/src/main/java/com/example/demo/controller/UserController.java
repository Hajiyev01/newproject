package com.example.demo.controller;

import com.example.demo.dto.LoginUserRequestDto;
import com.example.demo.dto.UserRequestDto;
import com.example.demo.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @PostMapping
    public Long saveUser(@RequestBody UserRequestDto userRequestDto) {
        return userService.saveUser(userRequestDto);
    }

    @PostMapping("/login")
    public String loginUser(@RequestBody LoginUserRequestDto loginUserRequestDto) {
        return userService.loginUser(loginUserRequestDto);
    }

    @PostMapping("/login2")
    public String loginUser2(@RequestBody LoginUserRequestDto loginUserRequestDto) {
        return userService.loginUser2(loginUserRequestDto);
    }



}
