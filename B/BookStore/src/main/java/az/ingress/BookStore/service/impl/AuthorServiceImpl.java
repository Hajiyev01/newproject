package az.ingress.BookStore.service.impl;

import az.ingress.BookStore.Configuration.Config;
import az.ingress.BookStore.dto.BookDto;
import az.ingress.BookStore.entity.Author;
import az.ingress.BookStore.entity.Book;
import az.ingress.BookStore.exception.UnAuthorizeException;
import az.ingress.BookStore.repository.AuthorRepository;
import az.ingress.BookStore.repository.BookRepository;
import az.ingress.BookStore.service.AuthorService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.nio.file.AccessDeniedException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {


    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;
    private final Config config;


    @Override
    public boolean isAuthorActive(Long authorId) {

        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new EntityNotFoundException("Author Not Faund."));
        return author.isActive();
    }

    @Override
    public void save(Long authorId, BookDto bookDto) throws UnAuthorizeException {
        boolean isAuthorActive = isAuthorActive(authorId);

        if (!isAuthorActive) {
            throw new UnAuthorizeException("Account not active.");
        }

        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new EntityNotFoundException("Author Not Faund."));

        Book book = config.getMapper().map(bookDto, Book.class);

        book.setAuthor(author);
        bookRepository.save(book);

    }

    @Override
    public void update(Long authorId, Long bookId, BookDto bookDto) throws UnAuthorizeException {
        boolean isAuthorActive = isAuthorActive(authorId);

        if (!isAuthorActive) {
            throw new UnAuthorizeException("Account not active.");
        }

        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new EntityNotFoundException("Author Not Faund."));
        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new EntityNotFoundException("Book Not Faund."));
        Book book1 = config.getMapper().map(bookDto, Book.class);

        bookRepository.save(book1);


    }

    @Override
    public void delete(Long authorId, Long bookId) throws UnAuthorizeException, AccessDeniedException {
        try {
            boolean isAuthorActive = isAuthorActive(authorId);

            if (!isAuthorActive) {
                throw new UnAuthorizeException("Account not active.");
            }

            Author author = authorRepository.findById(authorId)
                    .orElseThrow(() -> new EntityNotFoundException("Author Not Found."));
            Book book = bookRepository.findById(bookId)
                    .orElseThrow(() -> new EntityNotFoundException("Book Not Found."));
            if (!book.getAuthor().getId().equals(author.getId())) {
                throw new AccessDeniedException("Access denied.");
            }

            author.getBooks().remove(book);
            authorRepository.save(author);
            bookRepository.delete(book);
        } catch (AccessDeniedException e) {
            throw new AccessDeniedException("Access denied.");
        }
    }



    @Override
    public List<Book> getAllBooks(Long authorId) throws UnAuthorizeException {

        boolean isAuthorActive = isAuthorActive(authorId);

        if (!isAuthorActive) {
            throw new UnAuthorizeException("Account not active.");
        }

        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new EntityNotFoundException("Author Not Faund."));
        return author.getBooks();
    }
}
