package az.ingress.BookStore.service.impl;

import az.ingress.BookStore.Configuration.Config;
import az.ingress.BookStore.dto.RegisterDto;
import az.ingress.BookStore.entity.Author;
import az.ingress.BookStore.entity.Register;
import az.ingress.BookStore.entity.User;
import az.ingress.BookStore.repository.AuthorRepository;
import az.ingress.BookStore.repository.RegisterRepository;
import az.ingress.BookStore.repository.UserRepository;
import az.ingress.BookStore.service.RegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegisterServiceImpl implements RegisterService {


    private final RegisterRepository registerRepository;
    private final AuthorRepository authorRepository;
    private final UserRepository userRepository;
    private final Config config;

    @Override
    public void register(RegisterDto registerDto) {

        Register register = config.getMapper().map(registerDto, Register.class);
        registerRepository.save(register);

        if ("user".equals(registerDto.getUserType())) {
            User user = config.getMapper().map(registerDto, User.class);
            userRepository.save(user);

        } else if ("author".equals(registerDto.getUserType())) {
            Author author = config.getMapper().map(registerDto, Author.class);
            authorRepository.save(author);
        }



    }
}
